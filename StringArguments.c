#include <stdio.h>
#include <string.h>

//Function Prototypes
void append(char *mommystring, char *babystring);
void prepend(char *mommystring, char *babystring, int length);

int main(int argc, char *argv[])
{
    //checks to make sure that we have something to work with
    if(argc != 0)
    {
        char *findsamestring;
        //this is our long string like with the last program
        char mommystring[1000];
        //this sets all the values to null in our long string
        for(int i = 0; i < 1000; i++)
        {
            mommystring[i] = '\0';
        }
        
        //This is the loop to put the shorter strings into the long string
        for(int i = 1; i < argc; i++)
        {
            //searches through the long string to make sure that
            //there aren't any repeats
            findsamestring = strstr(mommystring, argv[i]);
            
            if(mommystring[0] == '\0')
            {
                strcpy(mommystring, argv[1]);
            } 
            
            if(findsamestring == NULL)
            {
                //Grabs the length for the smaller string
                int length = strlen(argv[i]);
            
                //This will decide whether to append or prepend
                int Compare = strcmp(mommystring, argv[i]);
                //The append check
                if(Compare < 0)
                {
                    append(mommystring, argv[i]);
                }
                //The prepend check
                if(Compare > 0)
                {
                    prepend(mommystring, argv[i], length);
                }
                //Print the whole string at the end of the loop
                printf("mommystring: '%s'\n", mommystring);
            }
        }
    }
}

void append(char *mommystring, char *babystring)
{
    strcat(mommystring, " ");
    strcat(mommystring, babystring);
}

void prepend(char *mommystring, char *babystring, int length)
{
    for (int i = strlen(mommystring); i >= 0; i--)
    {
        mommystring[i + length + 1] = mommystring[i];
    }
    strcpy(mommystring, babystring);
    mommystring[length] = ' ';
}
